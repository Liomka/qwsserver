#include <QCoreApplication>
#include <QTimer>

#include "websocketserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    WebSocketServer *webSocketServer = new WebSocketServer(&a);



    QObject::connect(webSocketServer, SIGNAL(finished()), &a, SLOT(quit()));
//    QTimer::singleShot(0, webSocketServer, SLOT(run()));

    return a.exec();
}
