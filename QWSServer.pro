#-------------------------------------------------
#
# Project created by QtCreator 2014-06-17T13:54:28
#
#-------------------------------------------------

QT       += core websockets

QT       -= gui

TARGET = QWebSocketServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    websocketserver.cpp

HEADERS += \
    websocketserver.h
