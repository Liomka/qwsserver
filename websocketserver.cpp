#include "websocketserver.h"

#include <QtWebSockets/QWebSocket>
#include <QtWebSockets/QWebSocketServer>

WebSocketServer::WebSocketServer(QObject *parent)
    : QObject(parent)
    , _webSocketServer(new QWebSocketServer("qchat", QWebSocketServer::NonSecureMode))
{

    connect(_webSocketServer, SIGNAL(closed()), this, SLOT(onClose()));
    connect(_webSocketServer, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
    connect(_webSocketServer, SIGNAL(serverError(QWebSocketProtocol::CloseCode)), this, SLOT(onServerError(QWebSocketProtocol::CloseCode)));

    _webSocketServer->listen();

    if (_webSocketServer->isListening()) {
        qDebug() << "Server is listenning on" << _webSocketServer->serverAddress().toString() + ":" + QString::number(_webSocketServer->serverPort());
    }

    qDebug() << "Constructed";
}

WebSocketServer::~WebSocketServer()
{
    qDebug() << "Destroyed .... NOOOO !";
}

// Server Behaviour
void WebSocketServer::onClose()
{
    qDebug() << "OnClose Event";
}

void WebSocketServer::onNewConnection()
{
    qDebug() << "NewConnection";
    //QWebSocket *client = _webSocketServer->nextPendingConnection();
    //connect(client, SIGNAL(textMessageReceived(QString)), this, SLOT(onTextReceived(QString)));
}

void WebSocketServer::onServerError(QWebSocketProtocol::CloseCode closeCode)
{
    qDebug() << "onServerError::triggered >>" << closeCode;
}

// Client
void WebSocketServer::onTextReceived(QString message)
{
    qDebug() << "Text received" << message;
}
