#ifndef SERVER_H
#define SERVER_H

#include <QtCore/QObject>
#include <QtWebSockets/QWebSocketServer>

class WebSocketServer : public QObject
{
    Q_OBJECT

public:
    explicit WebSocketServer(QObject *parent = 0);
    ~WebSocketServer();

signals:
    void finished();

private slots:
    void onClose();
    void onNewConnection();
    void onServerError(QWebSocketProtocol::CloseCode closeCode);

    void onTextReceived(QString message);

private:
    QWebSocketServer *_webSocketServer;

};

#endif // SERVER_H
